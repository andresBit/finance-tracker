class ApplicationController < ActionController::Base
  before_action :configure_permitted_parameters, if: :devise_controller?
  before_action :authenticate_user!

  protected

  def configure_permitted_parameters
    # deprecated
    # devise_parameter_sanitizer.for(:sign_up).push(:first_name, :last_name)
    # devise_parameter_sanitizer.for(:account_update).push(:first_name, :last_name)

    devise_parameter_sanitizer.permit(:sign_up, keys: [:first_name, :last_name, :avatar])
    devise_parameter_sanitizer.permit(:account_update, keys: [:first_name, :last_name, :avatar, :remove_avatar])
  end
end
